const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
    // watch: true,
    devServer: {
        contentBase: path.join(__dirname, 'build'),
        compress: true,
        port: 3346
    },
    watchOptions: {
        aggregateTimeout: 200,
        poll: 200,
        ignored: /node_modules/
    },
    entry: './src/assets/js/index.js',
    mode: 'development', // development - production
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'build')
    },
    plugins: [
        new CleanWebpackPlugin({
            verbose: false,
            cleanStaleWebpackAssets: false,
            protectWebpackAssets: false,
            cleanOnceBeforeBuildPatterns: [
                '**/*', // clean all
                '!views','!views/**/*', // ignore views folder
                '!lang','!lang/**/*',  // ignore lang folder
            ],
            cleanAfterEveryBuildPatterns: [
                'fonts/vendor' // remove fonts/vendor folder after build and watch
            ],
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css'
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/index.html'),
            filename: './index.html',
            cache: false,
            inject: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/map/index.html'),
            filename: './map/index.html',
            cache: false,
            inject: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/map/argus/index.html'),
            filename: './map/argus/index.html',
            cache: false,
            inject: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/map/azeroth/index.html'),
            filename: './map/azeroth/index.html',
            cache: false,
            inject: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/map/draenor/index.html'),
            filename: './map/draenor/index.html',
            cache: false,
            inject: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, './src/map/outland/index.html'),
            outputPath: './assets/fonts/',
            publicPath: './assets/fonts/',
            filename: 'a.html',
            cache: false,
            inject: true,
        })
    ],

    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-proposal-object-rest-spread']
                    }
                }
            },
            {
                test: /\.html$/,
                loader: 'html-loader',
            },
            {
                test: /\.s[ac]ss$/i,
                exclude: /node_modules/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(ico|jpg|jpeg|png|gif|webp|svg)(\?.*)?$/,
                // exclude: /node_modules/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './assets/img/',
                        publicPath: './assets/img/'
                    },
                },
            },
            {
                test: /\.(eot|otf|ttf|woff|woff2)(\?.*)?$/,
                // exclude: /node_modules/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: './assets/fonts/',
                        publicPath: './assets/fonts/'
                    },
                },
            },
        ]
    }
};
