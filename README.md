# Candy Project: World of Warcraft

## Technologies used:
<ul>
    <li>HTML</li>
    <li>FrameWork CSS: Tailwind</li>
    <li>SCSS</li>
    <li>JavaScript</li>
    <li>Webpack</li>
    <li>StyleLint</li>
    <li>PostCSS</li>
    <li>ESLint</li>
</ul>

### Installation
```
1 > git clone https://gitlab.com/baptiste.brand/world-of-warcraft.git
2 > cd world-of-warcraft
3 > npm install
4 > npm run build
```

### Preview
![img.png](img.png)
![img_1.png](img_1.png)
