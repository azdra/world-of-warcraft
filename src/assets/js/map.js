let mapname='';
if(location.href.includes('draenor')){
    mapname = 'draenor';
} else if(location.href.includes('outland')) {
    mapname = 'outland';
} else if(location.href.includes('argus')) {
    mapname = 'argus';
} else if(location.href.includes('azeroth')) {
    mapname = 'azeroth';
}

const image = `http://en.maps.dadesign.ru/${mapname}/data/`;
console.log(image)
const width = 21747;
const height = 12493;
const maxLevel = 7;
const minLevel = 3;
const orgLevel = 7;

// Some weird calculations to fit the image to the initial position
// Leaflet has some bugs there. The fitBounds method is not correct for large scale bounds
const tileWidth = 256 * Math.pow(2, orgLevel);
const radius = tileWidth / 2 / Math.PI;
const rx = width - tileWidth / 2;
const ry = -height + tileWidth / 2;

const west = -180;
const east = (180 / Math.PI) * (rx / radius);
const north = 85.05;
const south = (360 / Math.PI) * (Math.atan(Math.exp(ry / radius)) - (Math.PI / 4));
const rc = (tileWidth / 2 + ry) / 2;
const centerLat = (360 / Math.PI) * (Math.atan(Math.exp(rc / radius)) - (Math.PI / 4));
const centerLon = (west + east) / 2;
const bounds = [[south, west], [north, east]];

const map = new L.Map('mapid', {maxBounds: bounds});

L.tileLayer(image + '/{z}-{x}-{y}.jpg', {
    maxZoom: maxLevel, minZoom: minLevel, opacity: 1.0, zIndex: 1, noWrap: true,
    bounds: bounds,
    attribution: '&copy Blizzard Entertainment, compliled by <a href="mailto:dadesign@mail.ru">dadesign@mail.ru</a> v4.3'
}).addTo(map);

const zoom = map.getBoundsZoom(bounds);
const center = new L.latLng(centerLat, centerLon);

map.setView(center, zoom);